#include <stdlib.h>
#include <stdio.h>
#include <string.h> /* String function definitions */
#include <unistd.h> /* UNIX standard function definitions */
#include <fcntl.h> /* File control definitions */
#include <errno.h> /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <pthread.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>			//ionotify
#include <sys/iomsg.h>		//ionotify
#include <sys/neutrino.h> 	//MsgReceivePulse
#include <math.h>
#include <signal.h>
#include "ar_config.h"
#include "ar_recovery.h"
#include "ar_err.h"
#include "ar_err_codes.h"
#include "ar_debug.h"
#include "ar_shm_int.h"
#include "ar_eia232.h"
#include "ar_time.h"
#include "ar_shm_int.h"
#include "ar_shm_types.h"
#include "interfacesML_types.h"
#include "luaMAVSystemDefines.h"
#include "ha/ham.h"
/* Include files needed to use VnSensor. */
//#include "vn/sensors.h"
/* Include files needed to use the UART protocol. */
#include "util.h"
#include "upack.h"
#include "upackf.h"
#include "int.h"
/* This include file normally contains higher level abstractions for working
 * with VectorNav sensors. However, we include it for some string functions to
 * display named options for the sensor's register's fields. */
//#include "vn/sensors.h"

//#define REGISTER_HAM
//#define HAM_RESPAWN
//#define HAM_HEARTBEAT
#define IMU_RATE_250


//Definiciones para la configuraci�n del puerto serie
//#define BAUDRATE 115200L
#define BAUDRATE_OPERACION 230400L
#define IMU_BAUD_RATE 230400
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define RXBUFSIZE 4096
#define RESYNC 1

enum sensor_type {UNKNOWN, ORIENTUS, SPATIAL};
typedef struct{
	process_base_config_t proc;
	int baudrate;

	struct {
		int global_position;
		int gps_input;
		int attitude;
		int scaled_imu;
		int scaled_pressure;
		int attitude_quat;
	} shm;

	char dev[50];
	enum sensor_type type;
}sensorDriver_config_t;

sensorDriver_config_t cfg;
int glSerial;

//Variabled globales
int	AR_CalcThreadPriority;
struct termios oldtio,newtio;

//Forward declarations
int configPort(const char* path);
int Comms();
int Rx();
int serialSubscribe(int serialFd,struct sigevent *notifyEvent);

void asciiAsyncMessageReceived(void *userData, VnUartPacket *packet, size_t runningIndex);
void asciiOrBinaryAsyncMessageReceived(void *userData, VnUartPacket *packet, size_t runningIndex);
int processErrorReceived(char* errorMessage, VnError errorCode);

void packetFoundHandler(void *userData, VnUartPacket *packet, size_t runningIndexOfPacketStart);

int read_ar_config(char *name, int instance, sensorDriver_config_t *cfg)
{
	erret(init_config(name, instance, &cfg->proc));
	erret(read_config_int(cfg->proc.name,     "PRIORITY", (int *)&cfg->proc.priority));
	erret(read_config_int(cfg->proc.name,     "BAUDRATE", (int *)&cfg->baudrate));
	erret(read_config_string(cfg->proc.name,  "DEV", cfg->dev, sizeof(cfg->dev)));
	erret(read_config_int_tab(cfg->proc.name, "SHM_INDEX", "ATTITUDE",        &cfg->shm.attitude));
	erret(read_config_int_tab(cfg->proc.name, "SHM_INDEX", "SCALED_IMU",      &cfg->shm.scaled_imu));
	erret(read_config_int_tab(cfg->proc.name, "SHM_INDEX", "ATTITUDE_QUAT",   &cfg->shm.attitude_quat));
	return EXIT_SUCCESS;
}

static int init_SHM(sensorDriver_config_t *cfg)
{
	erret(SHM_Init());
	erret(SHM_InitSlot(cfg->shm.attitude,        sizeof(IML_ATTITUDE)));
	erret(SHM_InitSlot(cfg->shm.scaled_imu,      sizeof(IML_SCALED_IMU)));
	erret(SHM_InitSlot(cfg->shm.attitude_quat,   sizeof(IML_ATTITUDE_QUATERNION)));
	return EXIT_SUCCESS;
}

int main(int argc, char *argv[]) {
	/* Initialize the driver and start the driver threads */
	if(argc != 2)
	{
		perr(AR_ERR_BAD_INIT_PARAM);
		printf("Useage: %s INSTANCE\n", argv[0]);
		return(AR_ERR_BAD_INIT_PARAM);
	}
	/*	 * The next section inside #ifdef REGISTER_HAM is the code
	 * to register this SW component in the HAM
	 * and set the auto restar action if the process die
	 * and launch a termination script, that will cause the proces to die
	 * and restart, in the case of not being able to send one heartbeat to the
	 * HAM for more than one second
	 */
	#ifdef REGISTER_HAM
		ham_entity_t *ehdl; /* The entity Handle */
		ham_condition_t *chdl,*cdh2;
		ham_action_t *ahdl,*ahd2;

		ehdl = ham_attach_self("client17", 1000000000UL, 1, 2, 0);
		if (ehdl == NULL) {
		  printf("TERMINATING: Could not attach to Ham\n");
		  return EXIT_FAILURE;
		}
		else
		{
	#ifdef HAM_RESPAWN
			chdl = ham_condition(ehdl, CONDDEATH,"death",0);
			if(chdl==NULL)
			{
				printf("TERMINATING: Could not create condition 1\n");
				return EXIT_FAILURE;
			}
			else
			{
				//ahdl = ham_action_restart(chdl,"restart","/fs/sd0/uav/vn100_driver 1",HREARMAFTERRESTART);
				ahdl = ham_action_execute(chdl,"terminate","/fs/sd0/uav/restart_vn100_driver",HREARMAFTERRESTART);
			}
	#endif

	#ifdef HAM_HEARTBEAT
			cdh2 = ham_condition(ehdl, CONDHBEATMISSEDHIGH,"hbHigh",0);
			if(cdh2==NULL)
			{
				printf("TERMINATING: Could not create condition 2\n");
				return EXIT_FAILURE;
			}
			else
			{
			ahd2 = ham_action_execute(cdh2,"terminate","/fs/sd0/uav/terminate_vn100_driver",HREARMAFTERRESTART);
			}
	#endif
		}
	#endif
	read_ar_config(argv[0], atoi(argv[1]), &cfg);
	init_SHM(&cfg);

    ////CONFIGURACI�N PUERTO SERIE/////////////////
    {
		glSerial = open(cfg.dev, O_RDWR | O_NOCTTY );
		if (glSerial <0) {
			perror( "open" );
			exit(-1);
		}

		tcgetattr(glSerial,&oldtio); /* save current port settings */

		bzero(&newtio, sizeof(newtio));
		newtio.c_cflag = ((unsigned long) cfg.baudrate) | CS8 | CLOCAL | CREAD;
		cfsetispeed(&newtio, ((unsigned long) cfg.baudrate));
		cfsetospeed(&newtio, ((unsigned long) cfg.baudrate));
		newtio.c_iflag = IGNPAR;
		newtio.c_oflag = 0;
		/* set input mode (non-canonical, no echo,...) */
		newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
		newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused, 0.1xN seg */
		newtio.c_cc[VMIN]     = 16;   /* cero, separamos las tramas solo por el tiempo entre bytes  */
		tcflush(glSerial, TCIFLUSH);
		tcsetattr(glSerial,TCSANOW,&newtio);
    }
    /////////////////////////////////////////////////

	Rx();
	while(1)
	{

	}
	return EXIT_SUCCESS;
}

int Rx()
{
	//DECLARACIONES LOCALES
	int res;
	char buf[255];
    /* The VnUartProtocol structure encapsulates the data used for buffering
     * and handling incoming data. It is not associated with sending commands
     * to the sensor. This will be illustrated further in the example. */
    VnUartPacketFinder up;
    /* First thing you should do is initialize the data structure. */
    VnUartPacketFinder_initialize(&up);
    /* Register our callback method for when the VnUartPacketFinder finds an
       ASCII asynchronous packet. */
    VnUartPacketFinder_registerPacketFoundHandler(&up, packetFoundHandler, NULL);


    while (1) {       /* Bucle del programa */

    			res = read(glSerial,buf,16);   /* returns after  chars have been input */
    			if(res!=0)
    			{
    			    /* Now when we pass the data to the VnUartPacketFinder, our function
    			     * validPacketFoundHandler will be called since we passed in a complete
    			     * and valid data packet. Scroll down to the function validPacketFoundHandler
    			     * to see how to process and extract the values from the packet. */
    			    VnUartPacketFinder_processData(&up,(uint8_t*) buf, res);
    				memset(&buf,0,sizeof(buf));
    			}
    }
    //Dejamos puerto serie en configuraci�n inicial
    tcsetattr(glSerial,TCSANOW,&oldtio);
}



void packetFoundHandler(void *userData, VnUartPacket *packet, size_t runningIndexOfPacketStart)
{
    /* When this function is called, the packet will already have been
     * validated so no checksum/CRC check is required. */
    /* First see if this is an ASCII or binary packet. */
    if (VnUartPacket_type(packet) == PACKETTYPE_ASCII)
    {

    }
    else if (VnUartPacket_type(packet) == PACKETTYPE_BINARY) {

        uint64_t timeStartup;
        vec3f ypr;
        vec3f accels;
        vec3f rates;
        vec4f quat;
        IML_ATTITUDE attitude;
        IML_ATTITUDE_QUATERNION attitude_quat;
        IML_SCALED_IMU imu;
        static uint16_t     hb=0;

        /* See if this is a binary packet type we are expecting. */
        if (!VnUartPacket_isCompatible(
            packet,
             COMMONGROUP_YAWPITCHROLL |COMMONGROUP_QUATERNION |COMMONGROUP_ANGULARRATE | COMMONGROUP_ACCEL  ,
            TIMEGROUP_NONE,
            IMUGROUP_NONE,
            GPSGROUP_NONE,
            ATTITUDEGROUP_NONE,
            INSGROUP_NONE,
            GPSGROUP_NONE))
        {
            /* Not the type of binary packet we are expecting. */
            return;
        }
        /* Ok, we have our expected binary output packet. Since there are many
         * ways to configure the binary data output, the burden is on the user
         * to correctly parse the binary packet. However, we can make use of
         * the parsing convenience methods provided by the VnUartPacket structure.
         * When using these convenience methods, you have to extract them in
         * the order they are organized in the binary packet per the User Manual. */
//        timeStartup = VnUartPacket_extractUint64(packet);
        ypr = VnUartPacket_extractVec3f(packet);
        quat = VnUartPacket_extractVec4f(packet);
        rates = VnUartPacket_extractVec3f(packet);
        accels = VnUartPacket_extractVec3f(packet);

        attitude.state.usHeartBeat = hb;
        attitude.yaw=ypr.c[0]/180*M_PI;
        attitude.pitch=ypr.c[1]/180*M_PI;
        attitude.roll=ypr.c[2]/180*M_PI;
        attitude.rollspeed=rates.c[0];
        attitude.pitchspeed=rates.c[1];
        attitude.yawspeed=rates.c[2];
        attitude.state.uiDataOk = (isfinite(attitude.yaw) &&
        						   isfinite(attitude.pitch) &&
        						   isfinite(attitude.roll) &&
        						   isfinite(attitude.rollspeed) &&
        						   isfinite(attitude.pitchspeed) &&
        						   isfinite(attitude.yawspeed) ) ? SET_DATA_OK : SET_DATA_NOT_OK;

        imu.state.usHeartBeat = hb;
        imu.xacc=accels.c[0];
        imu.yacc=accels.c[1];
        imu.zacc=accels.c[2];
        imu.xgyro=rates.c[0];
        imu.ygyro=rates.c[1];
        imu.zgyro=rates.c[2];
        imu.state.uiDataOk = (isfinite(imu.xacc) &&
        					  isfinite(imu.yacc) &&
        					  isfinite(imu.zacc) &&
        					  isfinite(imu.xgyro) &&
        					  isfinite(imu.ygyro) &&
        					  isfinite(imu.zgyro)) ? SET_DATA_OK : SET_DATA_NOT_OK;

        memset(&attitude_quat, 0, sizeof(IML_ATTITUDE_QUATERNION));
        attitude_quat.state.usHeartBeat = hb;
        attitude_quat.q1 = quat.c[3]; /* scalar value */
        attitude_quat.q2 = quat.c[0]; /* X */
        attitude_quat.q3 = quat.c[1]; /* Y */
        attitude_quat.q4 = quat.c[2]; /* Z */
        attitude_quat.state.uiDataOk = (isfinite(attitude_quat.q1) &&
        								isfinite(attitude_quat.q2) &&
        								isfinite(attitude_quat.q3) &&
        								isfinite(attitude_quat.q4) ) ? SET_DATA_OK : SET_DATA_NOT_OK;

        SHM_WriteSlot(cfg.shm.attitude, (void *)&attitude, sizeof(IML_ATTITUDE));
        SHM_WriteSlot(cfg.shm.scaled_imu, (void *)&imu, sizeof(IML_SCALED_IMU));
        SHM_WriteSlot(cfg.shm.attitude_quat, (void *)&attitude_quat, sizeof(IML_ATTITUDE_QUATERNION));
        hb++;
#ifdef REGISTER_HAM
		ham_heartbeat();
#endif

    }
}


//int configPort(const char* path)
//{
//	int glSerial;
//
//			glSerial = open(path, O_RDWR | O_NOCTTY );
//			if (glSerial <0) {
//				perror( "open" );
//				exit(-1);
//			}
//
//		tcgetattr(glSerial,&oldtio); /* save current port settings */
//
//		bzero(&newtio, sizeof(newtio));
//		newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
//		newtio.c_cflag &= ~PARENB;
//		cfsetispeed(&newtio, BAUDRATE);
//		cfsetospeed(&newtio, BAUDRATE);
//		newtio.c_iflag = IGNPAR;
//		newtio.c_oflag = 0;
//		/* set input mode (non-canonical, no echo,...) */
//		newtio.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
//		newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused, 0.1xN seg */
//		newtio.c_cc[VMIN]     =0;   /* cero, separamos las tramas solo por el tiempo entre bytes  */
//
//		tcflush(glSerial, TCIFLUSH);
//		tcsetattr(glSerial,TCSANOW,&newtio);
//		return glSerial;
//}
