# This is an automatically generated record.
# The area between QNX Internal Start and QNX Internal End is controlled by
# the QNX IDE properties.

ifndef QCONFIG
QCONFIG=qconfig.mk
endif
include $(QCONFIG)

#===== USEFILE - the file containing the usage message for the application. 
USEFILE=

#===== EXTRA_INCVPATH - a space-separated list of directories to search for include files.
EXTRA_INCVPATH+=$(PROJECT_ROOT_ar_uti)  \
	$(PROJECT_ROOT_ar_shm) $(PROJECT_ROOT_ar_queue)  \
	$(PROJECT_ROOT)/../../comms/mavlink/include  \
	$(PROJECT_ROOT)/../../framework/interfacesmatlabml2017_act/interfacesML_ert_rtw  \
	$(PROJECT_ROOT)/../../framework/uavsystemdefines_act  \
	$(PROJECT_ROOT)/../../framework/qnxframework2017b/ar_queue  \
	$(PROJECT_ROOT)/../../framework/qnxframework2017b/ar_uti  \
	$(PROJECT_ROOT)/../../framework/qnxframework2017b/ar_shm

#===== CCFLAGS - add the flags to the C compiler command line. 
CCFLAGS+=-D__linux__
CCFLAGS_r+=REGISTER_HAM HAM_HEARTBEAT HAM_RESPAWN

#===== EXTRA_LIBVPATH - a space-separated list of directories to search for library files.
EXTRA_LIBVPATH+=$(PROJECT_ROOT_ar_queue)/$(CPU)/$(patsubst o%,a%,$(notdir $(CURDIR)))  \
	$(PROJECT_ROOT_ar_shm)/$(CPU)/$(patsubst o%,a%,$(notdir $(CURDIR)))  \
	$(PROJECT_ROOT_ar_uti)/$(CPU)/$(patsubst o%,a%,$(notdir $(CURDIR)))  \
	$(PROJECT_ROOT)/../../framework/qnxframework2017b/ar_queue/$(CPU)/$(patsubst o%,a%,$(notdir $(CURDIR)))  \
	$(PROJECT_ROOT)/../../framework/qnxframework2017b/ar_shm/$(CPU)/$(patsubst o%,a%,$(notdir $(CURDIR)))  \
	$(PROJECT_ROOT)/../../framework/qnxframework2017b/ar_uti/$(CPU)/$(patsubst o%,a%,$(notdir $(CURDIR)))

#===== LIBS - a space-separated list of library items to be included in the link.
LIBS+=-Bstatic ^ar_queue ^ar_shm ^ar_uti -Bdynamic ph  \
	phexlib ha ham -Bstatic m -Bdynamic

include $(MKFILES_ROOT)/qmacros.mk

#===== BUILDNAME - exact name (before appending suffixes like .a, .so) of build goal.
BUILDNAME_r=vn100driver_act
BUILDNAME_g=vn100driver_act_g

ifndef QNX_INTERNAL
QNX_INTERNAL=$(PROJECT_ROOT)/.qnx_internal.mk
endif
include $(QNX_INTERNAL)

include $(MKFILES_ROOT)/qtargets.mk

OPTIMIZE_TYPE_g=none
OPTIMIZE_TYPE=$(OPTIMIZE_TYPE_$(filter g, $(VARIANTS)))

